package `1`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val word = scanner.nextLine()
    print(toMutListOfChar(word))
}

fun toMutListOfChar(word:String):MutableList<Char>{
    val stringToMutList= mutableListOf<Char>()
    for (i in word) stringToMutList.add(i)

    return stringToMutList
}