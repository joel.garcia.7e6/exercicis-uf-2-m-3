package `1`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val input1 = scanner.next()
    val input2 = scanner.next()
    print(equalStringsChecker(input1,input2))
}

fun equalStringsChecker(input1:String, input2:String):Boolean{
    return input1 == input2
}