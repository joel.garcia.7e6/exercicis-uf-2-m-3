package `1`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val space = scanner.nextInt()
    val numOfCharacters = scanner.nextInt()
    val character = scanner.next().single()
    lineWriter(space,numOfCharacters,character)
}

fun lineWriter(space:Int,numOfCharacters:Int,character:Char){
    for (i in 1..space) {
        print(" ")
    }
    for (i in 1..numOfCharacters) {
        print(character)
    }
}

