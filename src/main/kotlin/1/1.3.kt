package `1`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val word = scanner.nextLine()
    val position = scanner.nextInt()
    print(foundCharInString(word, position))
}

fun foundCharInString(word:String, position:Int):String{
    if(position>word.length) return "La mida de l'String és inferior a $position"
    else return word[position].toString()

}