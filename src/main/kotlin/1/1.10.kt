package `1`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val sentence = scanner.nextLine()
    val character = scanner.next().single()
    mark(sentence,character)
}


fun mark(sentence:String,character:Char){
    repeat(sentence.length+4){
        print(character)
    }
    println()
    print(character)
    repeat(sentence.length+2){
        print(" ")
    }
    println(character)
    println("$character $sentence $character")
    print(character)
    repeat(sentence.length+2){
        print(" ")
    }
    println(character)
    repeat(sentence.length+4){
        print(character)
    }

}