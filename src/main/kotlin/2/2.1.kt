package `2`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    print(fact(num))
}

fun fact(num:Int):Long{
    return if(num==1) num.toLong()
    else num*fact(num-1)
}