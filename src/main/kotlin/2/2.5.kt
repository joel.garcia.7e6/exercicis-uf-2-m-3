package `2`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    print(redDig(num))
}

fun redDig(num:Int):Int {
    return if(num/10>0)redDig(sumaDig(num))
    else (num)
}
fun sumaDig(num:Int):Int{
    return if(num/10>0) num%10+sumaDig(num/10)
    else num
}
