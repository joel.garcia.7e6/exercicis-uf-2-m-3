package `2`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    print(numDig(num))
}

fun numDig(num:Int):Int{
    if(num < 10) { //caso base
        return 1
    } else {
        return 1 + numDig(num / 10)
    }
}