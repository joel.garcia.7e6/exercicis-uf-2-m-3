package `2`

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()
    print(augChecker(num))
}

fun augChecker(num:Int): Boolean {
   if(num/10>0) return num%10>num/10%10 && augChecker(num/10)
    else return true
}